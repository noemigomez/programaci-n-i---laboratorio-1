#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

#retorna promedio (suma / largo)
def promedio(notas):
    return (sum(notas) / len(notas))

#retorna el cuadrado de cada elemento
def cuadrados(reales):
    #se inicia la lista vacía y se van agregando los valores
    cuadrado = []
    for i in range (len(reales)):
        cuadrado.append(reales[i] ** 2)
    return cuadrado

#retorna la palabra mayor
def cuantas_letras(oracion):
    largo = 0
    #recorre los elementos y ve el más largo
    for i in range(len(oracion)):
        if len(oracion[i]) > largo:
            largo = len(oracion[i])
            indice = i
    return oracion[indice]

#main
#se definen listas y se imprimen con lo que se retorna
notas = [7.0, 5.6, 6.5, 4.0, 4.5, 3.0]
reales = [1, 3, 4, 6, 8, 9, 11, 15]
oracion = ['super', 'califragilistico', 'espiralidoso', 'supercalifragilisticoespiralidoso']
print('Las notas son: ', notas)
print('El promedio de las notas es: ', promedio(notas))
print('La lista de numeros enteros es:', reales)
print('La lista de los cuadrados es: ', cuadrados(reales))
print('La lista con contiene las siguientes palabras: ', oracion)
print('La palabra más larga es: ', cuantas_letras(oracion))
