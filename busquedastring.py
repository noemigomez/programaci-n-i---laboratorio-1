#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

#evalua si una letra es vocal
def es_vocal(letra):
    vocales = ['A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u']
    for i in range (10):
        if letra == vocales[i]:
            return True
            break
    return False

#retorna cantidad de vocales y consonantes
def cuantas_consonantes_vocales(palabra):
    #contadores en 0
    vocal = 0
    consonante = 0
    for i in range (len(palabra)):
        veracidad = es_vocal(palabra[i])
        if veracidad == True:
            vocal += 1
        else:
            consonante += 1
    return vocal, consonante

#main
#para ciclo
a = True
while a:
    letra = str(input('Ingrese letra: '))
    #ve si es vocal
    veracidad = es_vocal(letra)
    if veracidad == True :
        print('Es una vocal')
    else :
        print('Es una consonante')
    #pregunta si quiere colcar otra letra
    s = input('¿Quiere evaluar otra letra? (s/n): ')
    if s.upper() == 'N':
        a = False

#cuando termine el ciclo pide una palabra
palabra = str(input('AHORA \nIngrese una palabra: '))
vocal, consonante = cuantas_consonantes_vocales(palabra)
print('Vocales: ', vocal)
print('Consonantes: ', consonante)
